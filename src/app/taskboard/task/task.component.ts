import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task, States } from '../taskboard.model';

@Component({
  selector: 'app-task',
  templateUrl: 'task.component.html',
  styleUrls: ['task.component.css']
})
export class TaskComponent implements OnInit {
  @Input()
  get task(): Task {
    return this._task;
  }
  set task(task: Task) {
    if (task === null || task === undefined) {
      throw Error('TaskComponent: input property "task" should have a value');
    }
    this._task = task;
    switch (this._task.state) {
      case States.PLANNED:
        this._class = this.Primary;
        break;
      case States.IN_PROGRESS:
        this._class = this.Info;
        break;
      case States.COMPLETED:
        this._class = this.Success;
        break;
      default:
        this._class = this.Primary;
        break;
    }
  }

  @Output() onTaskClick: EventEmitter<number> = new EventEmitter<number>();
  @Output() onRemoveClick: EventEmitter<number> = new EventEmitter<number>();
  _task: Task;
  _class: string;
  private readonly Primary: string = 'primary';
  private readonly Info: string = 'info';
  private readonly Success: string = 'success';

  constructor() {}

  ngOnInit() {}

  onClick($event) {
    this.onTaskClick.emit(this._task.id);
  }

  removeCard($event) {
    $event.stopPropagation();
    this.onRemoveClick.emit(this._task.id);
  }
}
