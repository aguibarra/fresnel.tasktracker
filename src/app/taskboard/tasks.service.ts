import { Injectable } from '@angular/core';
import { Task, States, TasksList, CurrentStatus } from './taskboard.model';
import { Observable, of, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class TasksService {
  private counterID = 100;
  private tasksList: TasksList[] = [];
  $tasksLists: ReplaySubject<TasksList[]> = new ReplaySubject<TasksList[]>(1);

  public tasks: Task[] = [
    new Task(
      1,
      'Responsive',
      'Etiam porta sem malesuada magna mollis euismod.',
      States.PLANNED,
      5
    ),
    new Task(
      2,
      'QA Testing',
      'Etiam porta sem malesuada magna mollis euismod.',
      States.PLANNED,
      6
    ),
    new Task(
      3,
      'Budget',
      'Etiam porta sem malesuada magna mollis euismod.',
      States.PLANNED,
      8
    ),
    new Task(
      4,
      'Kickstart meeting',
      'Etiam porta sem malesuada magna mollis euismod.',
      States.PLANNED,
      2
    ),
    new Task(
      5,
      'Navigation',
      'Etiam porta sem malesuada magna mollis euismod.',
      States.IN_PROGRESS,
      6
    ),
    new Task(
      6,
      'Bootstrap 4',
      'Etiam porta sem malesuada magna mollis euismod.',
      States.IN_PROGRESS,
      9
    ),
    new Task(
      7,
      'Angular 5',
      'Etiam porta sem malesuada magna mollis euismod.',
      'Completed',
      5
    ),
    new Task(
      8,
      'Fields',
      'Etiam porta sem malesuada magna mollis euismod.',
      'Completed',
      4
    ),
    new Task(
      9,
      'Task board',
      'Etiam porta sem malesuada magna mollis euismod.',
      'Completed',
      56
    )
  ];

  constructor() {
    this.updateTasksList(this.tasks);
  }

  private reloadTasksList() {
    this.tasks = [];
    this.tasksList.forEach(element => {
      this.tasks = this.tasks.concat(element.tasks);
      element.tasks = [];
    });
    this.updateTasksList(this.tasks);
  }

  private updateTasksList(tasks: Task[]) {
    this.tasksList = [];
    this.tasksList.push({
      id: 1,
      state: States.PLANNED,
      tasks: tasks.filter(t => t.state === States.PLANNED),
      iconClass: 'ft-list'
    });

    this.tasksList.push({
      id: 2,
      state: States.IN_PROGRESS,
      tasks: tasks.filter(t => t.state === States.IN_PROGRESS),
      iconClass: 'ft-trending-up'
    });
    this.tasksList.push({
      id: 3,
      state: States.COMPLETED,
      tasks: tasks.filter(t => t.state === States.COMPLETED),
      iconClass: 'ft-thumbs-up'
    });
    this.$tasksLists.next(this.tasksList);
  }

  addNewTask(title: string, message: string) {
    this.tasksList.forEach(tl => {
      if (tl.state === States.PLANNED) {
        tl.tasks.push(
          new Task(this.counterID++, title, message, States.PLANNED, 0)
        );
      }
    });
    this.$tasksLists.next(this.tasksList);
  }

  removeTask(id: number) {
    for (let index = 0; index < this.tasksList.length; index++) {
      const list = this.tasksList[index];
      const task = list.tasks.find(t => t.id === id);
      if (task) {
        list.tasks = list.tasks.filter(t => t.id !== id);
        break;
      }
    }
    this.$tasksLists.next(this.tasksList);
  }

  getListsWithCards(): Observable<TasksList[]> {
    return this.$tasksLists;
  }

  getTask(id: number): Observable<Task> {
    return of(this.findTask(id));
  }

  getStates(): Observable<string[]> {
    return of(this.tasksList.map(a => a.state));
  }

  updateTask(task: Task): Observable<Task> {
    const taskFound = this.findTask(task.id);
    taskFound.id = task.id;
    taskFound.name = task.name;
    taskFound.state = task.state;
    taskFound.estimate = task.estimate;
    taskFound.description = task.description;
    this.reloadTasksList();
    return of(taskFound);
  }

  updateState(tasks: Task[], state: string): Observable<void> {
    this.tasksList.forEach(t => {
      if (t.state === state) {
        t.tasks = tasks;
        t.tasks.forEach(st => (st.state = state));
      }
    });
    this.$tasksLists.next(this.tasksList);
    return of();
  }

  getCurrentStatus(): Observable<CurrentStatus[]> {
    return this.$tasksLists.pipe(
      map(aux => {
        let totalTasks = 0;
        const totalDuration = aux.reduce((a, b) => {
          totalTasks += b.tasks.length;
          return a + b.tasks.reduce((c, d) => c + d.estimate, 0);
        }, 0);
        return aux.map(tl => {
          return {
            state: tl.state,
            percentValue: Math.round(
              isNaN(
                (tl.tasks.reduce((c, d) => c + d.estimate, 0) * 100) /
                  totalDuration
              )
                ? 0
                : (tl.tasks.reduce((c, d) => c + d.estimate, 0) * 100) /
                    totalDuration
            ),
            currentTasks: '(' + tl.tasks.length + ' / ' + totalTasks + ')',
            hours: tl.tasks.reduce((a, b) => a + b.estimate, 0),
            colorClass:
              tl.state === States.COMPLETED
                ? 'success'
                : tl.state === States.IN_PROGRESS
                ? 'info'
                : 'primary'
          };
        });
      })
    );
  }

  private findTask(id) {
    for (let index = 0; index < this.tasksList.length; index++) {
      const list = this.tasksList[index];
      const task = list.tasks.find(t => t.id === id);
      if (task) {
        return task;
      }
    }
    throw Error('No task has been found with id: ' + id);
  }
}
