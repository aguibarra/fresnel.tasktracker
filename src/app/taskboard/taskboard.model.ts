export class States {
  public static readonly PLANNED = 'Planned';
  public static readonly IN_PROGRESS = 'In Progress';
  public static readonly COMPLETED = 'Completed';
}

export class Task {
  public id: number;
  public name: string;
  public description: string;
  public state: string;
  public estimate: number;

  constructor(
    id: number,
    name: string,
    description: string,
    state: string,
    estimate: number
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.state = state;
    this.estimate = estimate;
  }
}

export class TasksList {
  id: number;
  state: string;
  tasks: Task[];
  iconClass: string;
  constructor(id: number, state: string, tasks: Task[], iconClass: string) {
    this.id = id;
    this.state = state;
    this.tasks = tasks;
    this.iconClass = iconClass;
  }
}

export class CurrentStatus {
  state: string;
  currentTasks: string;
  percentValue: number;
  hours: number;
  colorClass: string;
  constructor() {}
}
