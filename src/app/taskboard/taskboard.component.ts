import {
  Component,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  OnInit,
  OnDestroy
} from '@angular/core';
import { TasksService } from './tasks.service';
import { Task, TasksList, CurrentStatus } from './taskboard.model';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaskModalComponent } from './task-modal/task-modal.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TaskboardComponent implements OnInit, OnDestroy {
  @ViewChild('nameElement') nameElement: ElementRef;
  public tasksLists: TasksList[] = [];
  public currentStatuses: CurrentStatus[] = [];
  private _subscriptions: Subscription = new Subscription();
  public taskForm: FormGroup;

  constructor(
    public tasksService: TasksService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder
  ) {}
  ngOnInit(): void {
    this._subscriptions.add(
      this.tasksService
        .getCurrentStatus()
        .subscribe(c => (this.currentStatuses = c))
    );
    this.buildForm();
  }

  ngOnDestroy(): void {
    this._subscriptions.unsubscribe();
  }
  buildForm() {
    this.taskForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  addTask() {
    if (this.taskForm.valid) {
      this.tasksService.addNewTask(
        this.taskForm.controls.name.value,
        this.taskForm.controls.description.value
      );
    }
    this.taskForm.reset();
    this.nameElement.nativeElement.focus();
  }
  removeTask(id: number) {
    this.tasksService.removeTask(id);
  }

  viewTask(id: number) {
    const modalRef = this.modalService.open(TaskModalComponent);
    modalRef.componentInstance.id = id;
  }

  onDrag(tasks: Task[], state: string) {
    this.tasksService.updateState(tasks, state).subscribe();
  }
}
