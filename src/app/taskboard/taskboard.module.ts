import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula';
import { TaskboardRoutingModule } from './taskboard-routing.module';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { TaskboardComponent } from './taskboard.component';
import { TaskComponent } from './task/task.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TaskboardRoutingModule,
    DragulaModule,
    NgbProgressbarModule,
    ReactiveFormsModule
  ],
  declarations: [TaskboardComponent, TaskComponent]
})
export class TaskboardModule {}
