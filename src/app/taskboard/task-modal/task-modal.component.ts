import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TasksService } from '../tasks.service';
import { Task } from '../taskboard.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-task-modal',
  templateUrl: './task-modal.component.html'
})
export class TaskModalComponent {
  taskForm: FormGroup;
  _id: number;
  _task: Task;
  @Input()
  get id(): number {
    return this._id;
  }
  set id(value: number) {
    this._id = value;
    this.tasksService.getTask(this._id).subscribe(t => {
      this._task = t;
      this.taskForm.patchValue({
        name: this._task.name,
        description: this._task.description,
        estimate: this._task.estimate,
        state: this._task.state
      });
    });
  }
  constructor(
    public activeModal: NgbActiveModal,
    public tasksService: TasksService,
    private formBuilder: FormBuilder
  ) {
    this.buildForm();
  }

  private buildForm(): void {
    this.taskForm = this.formBuilder.group({
      name: [''],
      description: [''],
      estimate: [0, Validators.pattern('^[0-9]*$')],
      state: ['']
    });
  }

  save() {
    const task: Task = {
      id: this._task.id,
      name: this.taskForm.controls.name.value,
      description: this.taskForm.controls.description.value,
      state: this.taskForm.controls.state.value,
      estimate: +this.taskForm.controls.estimate.value
    };
    this.tasksService.updateTask(task).subscribe(c => {
      this.activeModal.close();
    });
  }
  remove() {
    this.tasksService.removeTask(this._id);
    this.activeModal.close();
  }
}
