import { Routes } from '@angular/router';

export const Full_ROUTES: Routes = [
  {
    path: 'taskboard',
    loadChildren: './taskboard/taskboard.module#TaskboardModule'
  },
  {
    path: '',
    redirectTo: 'taskboard',
    pathMatch: 'full'
  }
];
