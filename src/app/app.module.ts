import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full/full-layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DragulaModule } from 'ng2-dragula';
import { RoutesService } from './shared/sidebar/sidebar-routes.config';
import { TaskModalComponent } from './taskboard/task-modal/task-modal.component';
import { TasksService } from './taskboard/tasks.service';

@NgModule({
  declarations: [AppComponent, FullLayoutComponent, TaskModalComponent],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    HttpClientModule,
    DragulaModule.forRoot(),
    ToastrModule.forRoot(),
    NgbModule.forRoot()
  ],
  providers: [RoutesService, TasksService],
  bootstrap: [AppComponent],
  entryComponents: [TaskModalComponent]
})
export class AppModule {}
